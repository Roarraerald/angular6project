import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  friends: User [];

  constructor() {
    let myUser1: User = {
      nick: 'Ricardo1',
      subnick: '_Perez',
      age: 20,
      email: 'mi@gmail.com',
      friend: true,
      uid: 1
    };
    let myUser2: User = {
      nick: 'Lucas2',
      subnick: '_Perez',
      age: 20,
      email: 'mi@gmail.com',
      friend: true,
      uid: 2
    };
    let myUser3: User = {
      nick: 'Lucas3',
      subnick: '_Perez',
      age: 20,
      email: 'mi@gmail.com',
      friend: false,
      uid: 3
    };
    let myUser4: User = {
      nick: 'Lucas4',
      subnick: '_Perez',
      age: 20,
      email: 'mi@gmail.com',
      friend: true,
      uid: 4
    }
  this.friends = [myUser1, myUser2, myUser3, myUser4]
  }
  getFriends() {
    return this.friends
  }
}
